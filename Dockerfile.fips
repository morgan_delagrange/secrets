FROM registry.gitlab.com/gitlab-org/gitlab-runner/go-fips:1.18 AS build

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM registry.access.redhat.com/ubi8-minimal
USER root

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-8.18.1}

ADD https://github.com/zricethezav/gitleaks/releases/download/v${SCANNER_VERSION}/gitleaks_${SCANNER_VERSION}_linux_x64.tar.gz  /tmp/gitleaks.tar.gz

RUN microdnf update --disableplugin=subscription-manager --nodocs && \
    microdnf install wget shadow-utils util-linux tar git --disableplugin=subscription-manager --nodocs && \
    \
    # Install GitLeaks
    tar -xf /tmp/gitleaks.tar.gz -C /usr/local/bin/ && \
    chmod a+x /usr/local/bin/gitleaks && \
    rm -rf /tmp/gitleaks.tar.gz && \
    \
    # Configure CA Certificates
    # Ref: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-shared-system-certificates
    \
    mkdir -p /etc/pki/ca-trust/source/anchors/ && \
    ## Change the certs owner to the user
    touch /etc/pki/ca-trust/source/anchors/ca-certificates.crt && \
    \
    ## Change the certs owner to the user
    chmod -R g+w /etc/pki/ && \
    \
    # Create gitlab user
    useradd --create-home gitlab -g root && \
    \
    # Cleanup libs that are no longer needed
    microdnf remove shadow-utils tar && \
    microdnf clean all && \
    microdnf remove microdnf

COPY --from=build /go/src/app/analyzer /
COPY /gitleaks.toml /gitleaks.toml
USER gitlab

ENTRYPOINT []
CMD ["/analyzer", "run"]
